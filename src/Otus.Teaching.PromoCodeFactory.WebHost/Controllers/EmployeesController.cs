﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Exceptions;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        private readonly IEmployeeRequestMapper _employeeRequestMapper;
        
        private readonly IEmployeeResponseMapper _employeeResponseMapper;

        public EmployeesController(
            IRepository<Employee> employeeRepository,
            IEmployeeRequestMapper employeeRequestMapper, 
            IEmployeeResponseMapper employeeResponseMapper)
        {
            _employeeRepository = employeeRepository;
            _employeeRequestMapper = employeeRequestMapper;
            _employeeResponseMapper = employeeResponseMapper;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();
            return  employees
                .Select(x => _employeeResponseMapper.MapShort(x))
                .ToList();
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var model = await _employeeRepository.GetByIdAsync(id);

            if (model == null)
            {
                return NotFound();
            }
            
            return _employeeResponseMapper.Map(model);
        }

        /// <summary>
        /// Создать нового сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        //[ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<EmployeeResponse>> Create(EmployeeRequest employee)
        {
            var model = await _employeeRepository.CreateAsync(
                _employeeRequestMapper.Map(employee));
            
            return Ok(_employeeResponseMapper.Map(model));
            
            // Этот код не заработал. Пробовал подключить верcионность - тоже не получилось :(
            // Ошибка System.InvalidOperationException: No route matches the supplied values.
            // return CreatedAtAction(nameof(GetEmployeeByIdAsync), new {id = result.Id, }, response);
        }
        
        /// <summary>
        /// Удалить существующего сотрудника
        /// </summary>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> Delete(Guid id)
        {
            try
            {
                await _employeeRepository.DeleteAsync(id);
            }
            catch (Exception ex) when (
                ex is MoreThanOneEntityFoundRepositoryException ||
                ex is EntityNotFoundRepositoryException)
            {
                return NotFound();
            }
            
            return NoContent();
        }
        
        /// <summary>
        /// Обновить существующего сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> Update(Guid id, EmployeeRequest employee)
        {
            try
            {
                await _employeeRepository.UpdateAsync(id, _employeeRequestMapper.Map(id, employee));
            }
            catch (Exception ex) when (
                ex is MoreThanOneEntityFoundRepositoryException ||
                ex is EntityNotFoundRepositoryException)
            {
                return NotFound();
            }
            
            return NoContent();
        }
    }
}