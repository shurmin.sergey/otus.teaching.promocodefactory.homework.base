﻿using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public class EmployeeResponseMapper : IEmployeeResponseMapper
    {
        public EmployeeResponse Map(Employee employee)
        {
            return new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = $"{employee.FirstName} {employee.LastName}",
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };
        }
        
        public EmployeeShortResponse MapShort(Employee employee)
        {
            return new EmployeeShortResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                FullName = $"{employee.FirstName} {employee.LastName}",
            };
        }
    }
}