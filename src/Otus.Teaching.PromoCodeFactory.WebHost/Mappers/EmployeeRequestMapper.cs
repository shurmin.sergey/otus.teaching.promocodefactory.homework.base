﻿using System;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public class EmployeeRequestMapper : IEmployeeRequestMapper
    {
        public Employee Map(Guid id, EmployeeRequest request)
        {
            var destination = Map(request);
            destination.Id = id;
            return destination;
        }

        public Employee Map(EmployeeRequest request)
        {
            return new Employee()
            {
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                AppliedPromocodesCount = request.AppliedPromocodesCount,
                Roles = request.RoleIds?.Select(x => new Role()
                {
                    Id = x
                }).ToList()
            };
        }
    }
}