﻿using System;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public interface IEmployeeRequestMapper
    {
        Employee Map(Guid id, EmployeeRequest request);
        
        Employee Map(EmployeeRequest request);
    }
}