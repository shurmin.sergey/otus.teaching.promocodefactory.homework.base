﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public interface IEmployeeResponseMapper
    {
        EmployeeResponse Map(Employee employee);

       
        EmployeeShortResponse MapShort(Employee employee);
    }
}