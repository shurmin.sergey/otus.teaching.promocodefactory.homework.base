﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Exceptions;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        private readonly object _lockUpdateDeleteOperations = new object();
        
        private List<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data.ToList();
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data.AsEnumerable());
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<T> CreateAsync(T entity)
        {
            entity.Id = Guid.NewGuid();
            //TODO: Правильно реализовать добавление(сохранение) ролей с рповеркой на существование
            Data.Add(entity);
            return Task.FromResult(entity);
        }

        public Task UpdateAsync(Guid id, T entity)
        {
            ThrowExceptionIfNotUniqueExists(id);

            lock (_lockUpdateDeleteOperations)
            {
                for (var i = Data.Count - 1; i >= 0; i--)
                {
                    if (Data[i].Id == id)
                    {
                        Data[i] = entity;
                        break;
                    }
                }
            }
            
            return Task.CompletedTask;
        }

        public Task DeleteAsync(Guid id)
        {
            lock (_lockUpdateDeleteOperations)
            {
                ThrowExceptionIfNotUniqueExists(id);
                Data.RemoveAll(x => x.Id == id);
            }

            return Task.CompletedTask;
        }

        private void ThrowExceptionIfNotUniqueExists(Guid id)
        {
            var foundEntitiesCount = Data.Count(x => x.Id == id);

            if (foundEntitiesCount == 0)
            {
                throw new EntityNotFoundRepositoryException($"Сущность с идентификатором {id} не найдена")
                {
                    Source = nameof(InMemoryRepository<T>)
                };
            }
            else if (foundEntitiesCount > 1)
            {
                throw new MoreThanOneEntityFoundRepositoryException($"Найдено более одной сущности с идентификатором {id}")
                {
                    Source = nameof(InMemoryRepository<T>)
                };
            }
        }
    }
}