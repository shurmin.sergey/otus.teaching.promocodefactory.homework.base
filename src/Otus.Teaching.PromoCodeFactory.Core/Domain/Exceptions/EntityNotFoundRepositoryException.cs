﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Exceptions
{
    public class EntityNotFoundRepositoryException : Exception
    {
        public override string Message { get; }

        public EntityNotFoundRepositoryException(string message)
        {
            Message = message;
        }
    }
}