﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Exceptions
{
    public class MoreThanOneEntityFoundRepositoryException : Exception
    {
        public override string Message { get; }

        public MoreThanOneEntityFoundRepositoryException(string message)
        {
            Message = message;
        }
    }
}